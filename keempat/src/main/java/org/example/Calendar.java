package org.example;
import java.util.*;
import java.text.*;

import java.util.ArrayList;

public class Calendar {


    public final static String[] arrayBulan = {
            "unused", "Januari", "Februari", "Maret", "April", "Mei", "Juni",
            "Juli", "Agustus", "September", "Oktober", "November", "Desember"
    };

    public final static int[] arrayHari = {

            0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31

    };


    public void setMonth(int month, int year) {

        if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0) && month == 2) {
            arrayHari[month] = 29;
        }
        int blankdays = 0;
        System.out.println();
        System.out.println("======== " + arrayBulan[month] + " " + year + " ========");

        GregorianCalendar cldr = new GregorianCalendar(year, month, 1);
        System.out.println("[M] [S] [S] [R] [K] [J] [S]");

        blankdays = cldr.get(java.util.Calendar.DAY_OF_WEEK) - 1;
        int hariBulan = arrayHari[month];

        if (cldr.isLeapYear(cldr.get(java.util.Calendar.YEAR)) && month == 1) {
            ++hariBulan;
        }

        for (int i = 1; i <= blankdays; i++) {
            System.out.print("--  ");
        }

        for (int i = 1; i <= hariBulan; i++) {
            if (i <= 9) {
                System.out.print(" ");
            }
            System.out.print(i);


            if ((blankdays + i) % 7 == 0) {
                System.out.println();
            } else {
                System.out.print("  ");
            }
        }
    }
    public void setWeek(int month, int year, int week) {

        if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0) && month == 2) {
            arrayHari[month] = 29;
        }
        int blankdays = 0;

        System.out.println();
        System.out.println("======== " + arrayBulan[month] + " " + year + " ========");

        GregorianCalendar cldr = new GregorianCalendar(year, month, 1);
        System.out.println("[M] [S] [S] [R] [K] [J] [S]");

        blankdays = cldr.get(java.util.Calendar.DAY_OF_WEEK) - 1;
        int hariBulan = arrayHari[month];

        if (cldr.isLeapYear(cldr.get(java.util.Calendar.YEAR)) && month == 1) {
            ++hariBulan;
        }

        for (int i = 1; i <= blankdays; i++) {
            System.out.print("--  ");
        }

        for (int i = 1; i <= hariBulan; i++) {
            if (i <= 9) {
                System.out.print(" ");
            }
            if (i <= (week * 7)) {
                System.out.print(i);
            if (i >= (week * 7 + 1)){
                break;
            }
            }

            if ((blankdays + i) % 7 == 0) {
                System.out.println();
            } else {
                System.out.print("  ");
            }
        }
    }

}











